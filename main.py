from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5.QtCore import QTimer
from PyQt5.QtCore import Qt
from PyQt5.QtMultimedia import QSound
from PyQt5.QtGui import QIcon
import sys

import TableroWidget
from Limpia import Tablero, Agente

#TAM_X = 10
#TAM_Y = 10

class Window(QMainWindow):
    def __init__(self) -> None:
        super().__init__()

        self.setGeometry(300, 300, 600, 650)
        self.setWindowTitle("Limpiatron 5000")
        
        self.sonido_comer = QSound("sounds/chomp.wav")
        self.sonido_victoria = QSound ("sounds/app-29.wav")
        self.tablero_wid = TableroWidget.TableroWidget()
        self.tablero_wid.minimumWidth = 100
        self.tablero_wid.minimumHeight = 100
        self.setCentralWidget(self.tablero_wid)

        sim_toolbar = self.addToolBar('Simular')
        sim_toolbar.setMovable(False)
        sim_toolbar.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonTextBesideIcon)
        self.action_paso = sim_toolbar.addAction(QIcon('img/paso.png'), 'Paso × 1')
        self.action_sim = sim_toolbar.addAction(QIcon('img/arrow.png'), 'Simular')
        self.action_res = sim_toolbar.addAction(QIcon('img/fire.png'), 'Reiniciar')

        self.action_paso.triggered.connect(self.on_paso_triggered)
        self.action_sim.triggered.connect(self.on_simular_triggered)
        self.action_res.triggered.connect(self.on_reset_triggered)

        self.timer = QTimer(self)
        self.timer.setInterval(500)
        self.timer.timeout.connect(self.on_simular_timeout)
        self.activo = False

        self.reiniciar()

        self.show()

    def on_paso_triggered(self):
        print("Paso")
        self.simula_paso()

    def on_simular_triggered(self):
        print('Simular %d' % self.activo)
        if self.activo:
            self.timer.stop()
            self.activo = False
        else:
            self.timer.start()
            self.activo = True

    def on_reset_triggered(self):
        print('Reset')
        self.activo = False
        self.timer.stop()
        self.reiniciar()

    def on_simular_timeout(self):
        print('tick')
        self.simula_paso()

    def reiniciar(self):
        self.tablero = Tablero()
        self.agente = Agente(self.tablero)
        self.tablero_wid.tablero = self.tablero
        self.tablero_wid.agente = self.agente
        self.action_paso.setEnabled(True)
        self.action_sim.setEnabled(True)
        self.tablero_wid.update()

    def simula_paso(self):
        self.agente.mover_random()
        if self.agente.come_basura(self.tablero):
            print('Nam')
            self.sonido_comer.play()
        if self.tablero.checar_victoria():
            self.activo = False
            self.timer.stop()
            self.action_paso.setEnabled(False)
            self.action_sim.setEnabled(False)
            msg = QMessageBox(self)
            self.sonido_victoria.play()
            msg.setText('Victoria!!!11!!')
            msg.exec()

        self.tablero_wid.update()

def main():
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
