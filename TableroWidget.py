from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QPaintEvent, QResizeEvent, QPixmap
from PyQt5.QtCore import QRect
from Limpia import TAM_X, TAM_Y

class TableroWidget(QWidget):
    def __init__(self) -> None:
        super().__init__()
        self.tam_celda = 10

        self.basura_img1 = QPixmap()
        self.basura_img1.load('img/bolsa.png')
        self.basura_img2 = QPixmap()
        self.basura_img2.load('img/lata.png')
        self.agente_img = QPixmap()
        self.agente_img.load('img/cleaner.png')
        self.fondo = QPixmap()
        self.fondo.load('img/Vectorized_Bliss.svg')

    def resizeEvent(self, event: QResizeEvent) -> None:
        width = self.width() // TAM_X
        height = self.height() // TAM_Y
        self.tam_celda = min(width, height)

        self.update()

        return super().resizeEvent(event)

    def paintEvent(self, event: QPaintEvent) -> None:
        painter = QPainter(self)
        
        rect = QRect(0, 0, self.width(), self.height())
        painter.drawPixmap(rect, self.fondo)
        
        for i in range(TAM_X + 1):
            painter.drawLine(i * self.tam_celda, 0, i * self.tam_celda, TAM_Y * self.tam_celda)
        for j in range(TAM_Y + 1):
            painter.drawLine(0, j * self.tam_celda, TAM_X * self.tam_celda, j * self.tam_celda)
        
        for i in range(TAM_X):
            for j in range(TAM_Y):
                if self.tablero.tabla[i][j] == 1:
                    rect = QRect(i * self.tam_celda, j * self.tam_celda, self.tam_celda, self.tam_celda)
                    if((i + j) % 2 == 0):
                        painter.drawPixmap(rect, self.basura_img1)
                    else:
                        painter.drawPixmap(rect, self.basura_img2)

        rect = QRect(self.agente.posx * self.tam_celda, self.agente.posy * self.tam_celda, self.tam_celda, self.tam_celda)
        painter.drawPixmap(rect, self.agente_img)
