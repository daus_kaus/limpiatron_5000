import random
##Dimensiones del tablero:
TAM_X = 10
TAM_Y = 10
NUM_BASURA = 10
## Clase principal:

class Tablero ():
    tabla= 0
    contador = NUM_BASURA
    def __init__(self) -> None:
        self.tabla = []
        for i in range(TAM_X):
            col= []
            for j in range(TAM_Y):
                col.append (0)
            self.tabla.append(col)
        ##Generacion de basura
        i=0
        while (i<NUM_BASURA):
            rx = random.randint(0, TAM_X-1)
            ry = random.randint(0, TAM_Y-1)
            if (self.tabla [rx] [ry] == 0):
                self.tabla [rx] [ry] = 1
                i+=1
        pass
    def checar_victoria (self):
        if self.contador <= 0:
            return True
        else:
            return False

##Clase del roomba
class Agente ():
    posx=0
    posy=0
    def __init__(self, tbl) -> None:
        while (True):
            rx = random.randint(0, TAM_X-1)
            ry = random.randint(0, TAM_Y-1)
            if (tbl.tabla [rx][ry]==0):
                self.posx=rx
                self.posy=ry
                return
        pass
    def mover_random(self):

        vertical=0
        horizontal=0
        while((vertical == 0 and horizontal ==0)  or (self.posx+horizontal>=TAM_X or self.posx+horizontal<0) or (self.posy+vertical>=TAM_Y or self.posy+vertical<0)):
            vertical = random.randint(-1, 1)
            horizontal = random.randint(-1, 1)
        self.posx+=horizontal
        self.posy+=vertical
        return
    
    def come_basura (self, tbl):
        if (tbl.tabla [self.posx] [self.posy] == 1):
            tbl.tabla [self.posx] [self.posy] = 0
            tbl.contador -=1
            return True
        else:
            return False
